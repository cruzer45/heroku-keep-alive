/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mauricerogers.herokukeepalive;

import static com.mauricerogers.herokukeepalive.HerokuKeepAlive.log;
import java.io.IOException;
import org.jsoup.Jsoup;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author m.rogers
 */
public class Caller implements org.quartz.Job
{

    public Caller()
    {
    }

    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        String url = "http://intellinet.mauricerogers.com/";

        log.info("About to connect to the site.");
        try
        {
            Jsoup.connect(url).get();
            log.info("Successfully connected to the url: " + url);
        }
        catch (IOException ex)
        {
            log.error("Cannot connect to the site: " + url);
        }
    }
}
